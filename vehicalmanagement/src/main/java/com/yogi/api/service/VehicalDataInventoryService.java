package com.yogi.api.service;

import com.yogi.api.response.IOFileInformationResponseDTO;
import com.yogi.api.service.interfaces.IVehicalDataInventory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.activation.MimetypesFileTypeMap;
import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Created by Krishan Shukla on 18/09/2017.
 */
@Service("userService")
@Transactional
public class VehicalDataInventoryService implements IVehicalDataInventory{

    public Set<IOFileInformationResponseDTO> getFilesInDirectory(String dirLocation) throws IOException {
        Set<IOFileInformationResponseDTO> ioFileInfoSet = new HashSet<IOFileInformationResponseDTO>();
        IOFileInformationResponseDTO ioFileInfo = null;

        File file = new File(dirLocation);
        File[] files = file.listFiles();
        for(File currentFile: files){
            ioFileInfo = new IOFileInformationResponseDTO();
            ioFileInfo.setFileName(currentFile.getName());
            ioFileInfo.setFileMIMEType(Files.probeContentType(Paths.get(currentFile.getPath())));
            ioFileInfoSet.add(ioFileInfo);
        }

        return ioFileInfoSet;

    }
/*
        String fileType = "";
        File f = new File("");

        System.out.println("Mime Type of " + f.getName() + " is " +
                new MimetypesFileTypeMap().getContentType(f));

     *//* Stream<Path> mypaths =    Files.list(Paths.get(dirLocation));
        mypaths.map()*//*
        return null;
    }*/

}
