package com.yogi.api.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by Krishan Shukla on 18/09/2017.
 */
@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = "com.yogi")
public class VehicalManagmentApplication {
    public static void main(String[] args) {
        SpringApplication.run(VehicalManagmentApplication.class, args);
    }
}
