package com.yogi.api.controller;


import com.yogi.api.common.BaseController;
import com.yogi.api.request.IOFileInformationRequestDTO;
import com.yogi.api.response.IOFileInformationResponseDTO;
import com.yogi.api.service.interfaces.IVehicalDataInventory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Set;

import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;

/**
 * Created by Krishan Shukla on 18/09/2017.
 */

@RestController
@RequestMapping(value="/v1")
@PropertySource(value = {"classpath:properties.${ENV_SYSTEM:qa}/props-for-api-tests.properties", "classpath:properties.${ENV_SYSTEM:qa}/props-for-ui-tests.properties"})
public class VehicalDataInventoryController extends BaseController {

    private static Logger LOGGER = LoggerFactory.getLogger(VehicalDataInventoryController.class);
    RestTemplate restTemplate;

    @Autowired
    Environment environment;
    @Autowired
    IVehicalDataInventory vehicalDataInventory;

    /**
     * @api {post} /api/scanfiles scan the files in the given folder
     * @apiGroup VehicalManagement
     * @apiDescription Get the files scanned in folder to get vehical information
     *
     * @apiParam (Body) {Object} Body
     *
     * @apiSuccess (200) {IOFileInformationRequestDTO} return infomation of file
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 CREATED
     *       {}
     * @apiError
     * @apiErrorExample
     *
     */
    @RequestMapping(
            value = "/api/scanfiles",
            method = {RequestMethod.POST},
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Set<IOFileInformationResponseDTO> scanfiles(@RequestParam String directoryPath) throws IOException {
        LOGGER.info("Inside scanfile controller");
        Set<IOFileInformationResponseDTO> ioFileInfo = vehicalDataInventory.getFilesInDirectory(directoryPath);
        return ioFileInfo;
   }


}
